const express = require('express');
const passport = require('passport');
const AuthController = require('./controller/AuthController');
const router = express.Router();

router.get('/', AuthController.index);

// Route to handle the register get & post method
router.get('/register', AuthController.registerGet);
router.post('/register', AuthController.registerPost);

// Route to handle the login get & post method
router.get('/login', AuthController.loginGet);
router.post('/login', passport.authenticate('local', { failureRedirect: '/login', failureFlash: true }), AuthController.loginPost);

// Route to handle the logout
router.get('/logout', AuthController.logout);

// Sort Schedule
router.get('/sort', AuthController.sort);
router.get('/schedule', AuthController.schedule);

// Test Connection
router.get('/ping', AuthController.ping);

module.exports = router;
