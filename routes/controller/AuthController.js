const mongoose = require('mongoose');
const passport = require('passport');
const moment = require('moment');
const Account = require('../../models/account');

const controller = {
    index: (req, res) => {
        Account
            // .find({ 'teachingClass.class': '1' })
            .find()
            .sort({})
            .exec((err, user) => {
                if (err) {
                    console.log("Error:", err);
                } else {
                    let tableData = [];

                    user.forEach(el => {
                        tableData.push({
                            'title': el.teachingSubject + ' by ' + el.name.firstName + ' ' + el.name.lastName,
                            'start': el.teachingClass.start_class,
                            'end': el.teachingClass.end_class,
                            'class': el.teachingClass.class
                        });
                    });

                    res.render('index', {
                        title: 'iHRMS',
                        user: req.user,
                        users: user,
                        tableData: tableData,
                        error: req.flash('error')
                    });
                }
            });
    },
    loginGet: (req, res) => {
        res.render('login', { 
            title: 'iHRMS Login', 
            user: req.user, 
            error: req.flash('error'),
        });
    },
    loginPost: (req, res, next) => {
        req.session.save((err) => {
            if (err) {
                return next(err);
            }
            res.redirect('/admin');
        });
    },
    registerGet: (req, res) => {
        res.render('register', { title: 'iHRMS Register' });
    },
    registerPost: (req, res, next) => {
        Account.register(new Account({ username: req.body.username }), req.body.password, (err, account) => {
            if (err) {
                return res.render('register', { error: err.message });
            }

            passport.authenticate('local')(req, res, () => {
                req.session.save((err) => {
                    if (err) {
                        return next(err);
                    }
                    res.redirect('/admin');
                });
            });
        });
    },
    logout: (req, res, next) => {
        req.logout();
        req.session.save((err) => {
            if (err) {
                return next(err);
            }
            res.redirect('/');
        });
    },
    ping: (req, res) => {
        res.status(200).send("pong!");
    },
    sort: (req, res) => {
        Account
            .find({ 'teachingClass.class' : req.query.class})
            .sort({})
            .exec((err, val) => {
                if (err) {
                    console.log("Error:", err);
                } else {
                    let tableData = [];

                    val.forEach(el => {
                        tableData.push({
                            'name': el.name.firstName,
                            'class': el.teachingClass.class,
                            'teachingSubject': el.teachingSubject,
                            'start_class': moment(el.teachingClass.start_class).format('dddd, MMMM Do YYYY, h:mm:ss a'),
                            'end_class': moment(el.teachingClass.end_class).format('dddd, MMMM Do YYYY, h:mm:ss a')
                        });
                    });
                    
                    res.send(tableData);
                }
            });
    },
    schedule: (req, res) => {
        // Account
        //     .find({
        //         'teachingClass.class': req.query.id
        //     })
        //     .sort({})
        //     .exec((err, val) => {
        //         if (err) {
        //             console.log("Error:", err);
        //         } else {
        //             let tableData = [];

        //             val.forEach(el => {
        //                 tableData.push({
        //                     'title': el.teachingSubject + ' by ' + el.name.firstName + ' ' + el.name.lastName,
        //                     'start': el.teachingClass.start_class,
        //                     'end': el.teachingClass.end_class,
        //                     'class': el.teachingClass.class
        //                 });
        //             });

        //             res.send(tableData);
        //         }
        //     });
        if(req.query.id) {
            Account
                .find({
                    'teachingClass.class': req.query.id
                })
                .sort({})
                .exec((err, val) => {
                    if (err) {
                        console.log("Error:", err);
                    } else {
                        let tableData = [];

                        val.forEach(el => {
                            tableData.push({
                                'title': el.teachingSubject + ' by ' + el.name.firstName + ' ' + el.name.lastName,
                                'start': el.teachingClass.start_class,
                                'end': el.teachingClass.end_class,
                                'class': el.teachingClass.class
                            });
                        });

                        res.send(tableData);
                    }
                });
        } else {
            Account
                .find()
                .sort({})
                .exec((err, val) => {
                    if (err) {
                        console.log("Error:", err);
                    } else {
                        let tableData = [];
    
                        val.forEach(el => {
                            tableData.push({
                                'title': el.teachingSubject + ' by ' + el.name.firstName + ' ' + el.name.lastName,
                                'start': el.teachingClass.start_class,
                                'end': el.teachingClass.end_class,
                                'class': el.teachingClass.class
                            });
                        });
                        
                        res.send(tableData);
                    }
                });
        }
    }
}

module.exports = controller;