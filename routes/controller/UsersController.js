const mongoose = require('mongoose');
const passport = require('passport');
const Account = require('../../models/account');
const mailer = require('../../lib/sendMail');

const userController = {
    list: (req, res) => {
        if (!req.isAuthenticated()) {
            req.flash('error', 'Please login first before access admin page')
            res.redirect('/')
        } else {
            Account.find({}).exec((err, user) => {
                if (err) {
                    console.log("Error:", err);
                } else {
                    res.render('../views/admin', {
                        title: 'iHRMS Admin',
                        user: req.user,
                        users: user
                    });
                }
            });
        }
    },
    edit: (req, res) => {
        if(!req.isAuthenticated()) {
            req.flash('error', 'Please login first before access admin page')
            res.redirect('/')
        } else {
            Account.findOne({
                _id: req.params.id
            }).exec((err, user) => {
                if (err) {
                    console.log(err);
                } else {
                    res.render('../views/admin/edit', {
                        title: 'Edit - iHRMS Admin',
                        user: req.user,
                        users: user
                    });
                }
            })
        }
    },
    update: (req, res) => {
        Account.findById(req.params.id, function (err, user) {
            if(err) 
                res.send(err)
            
            user.name.firstName = req.body.firstName;
            user.name.lastName = req.body.lastName;
            user.address = req.body.address;
            user.email = req.body.email;
            user.phone = req.body.phone;
            user.position = req.body.position;
            user.teachingSubject = req.body.subject;
            user.isAdmin = false;

            user.teachingClass.class = req.body.classroom;
            user.teachingClass.start_class = req.body.start_class;
            user.teachingClass.end_class = req.body.end_class;

            // user.classes.class = req.body.classroom;
            // user.classes.schedule = req.body.schedule;

            // user.markModified('teachingCLass');
            user.save((err, user) => {
                if(err)
                    res.send(err)
                console.log(user, 'UPDATE SUCCESS')
                res.redirect('/admin');
            });
        });
    },
    delete: (req, res) => {
        Account.remove({
            _id: req.params.id
        }, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log("User deleted!");
                res.redirect("/admin");
            }
        });
    },
    mailbox: (req, res) => {
        if (!req.isAuthenticated()) {
            req.flash('error', 'Please login first before access admin page')
            res.redirect('/')
        } else {
            Account.find({}).exec((err, user) => {
                if (err) {
                    console.log("Error:", err);
                } else {
                    res.render('../views/admin/mailbox', {
                        title: 'iHRMS Mailbox',
                        user: req.user,
                        users: user
                    });
                }
            });
        }
    },
    email: (req, res) => {
        mailer.sendMail(req, res);
    }
};

module.exports = userController;