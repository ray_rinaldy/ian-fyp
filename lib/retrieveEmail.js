const fs = require('fs');
const Imap = require('imap');
const inspect = require('util').inspect;
const MailParser = require('mailparser').MailParser;

let imap = new Imap({
    user: 'ian.hrsystem@gmail.com',
    password: 'abcDE123@',
    host: 'imap.gmail.com',
    port: 993,
    tls: true
});

function openInbox(cb) {
    imap.openBox('INBOX', true, cb);
}

imap.once('ready', () => {
    openInbox((err, box) => {
        if (err) throw err;

        // let f = imap.fetch(box, {
        //     bodies: ''
        // })

        var f = imap.seq.fetch('1:3', {
            bodies: 'HEADER.FIELDS (FROM TO SUBJECT DATE)',
            struct: true
        });

        f.on('message', (msg, seqno) => {
            // console.log('Message #%d', seqno);
            let prefix = '(#' + seqno + ') ';

            let parser = new MailParser({
                streamAttachments: true
            });

            parser.on("headers", function (headers) {
                console.log("Header: " + JSON.stringify(headers));
            });

            parser.on("attachment", function (attachment) {
                console.log('writing attachment: ' + attachment.generatedFileName);
                var output = fs.createWriteStream(attachment.generatedFileName);
                attachment.stream.pipe(output);
            });

            parser.on('data', data => {
                if (data.type === 'text') {
                    console.log(seqno);
                    console.log(data.text); /* data.html*/
                }
            });

            parser.on('end', function (mail_object) {
                console.log('in mailParser::end');
            });

            msg.on('body', (stream, info) => {
                let buffer = '';

                stream.on('data', (chunk) => {
                    buffer += chunk.toString('utf8');
                });
                // stream.pipe(fs.createWriteStream('msg - ' + seqno + ' -body.txt'));
                stream.once('end', () => {
                    console.log(prefix + 'Parsed header: %s', inspect(Imap.parseHeader(buffer)));
                    parser.write(buffer);
                });
            });
            
            msg.once('attributes', (attrs) => {
                // console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
            });
            
            msg.once('end', () => {
                // console.log(prefix + 'Finished');
                parser.end();
            });
        });

        f.once('error', (err) => {
            console.log('Fetch error: ' + err);
        });

        f.once('end', () => {
            console.log('Done fetching all messages!');
            imap.end();
        });
    });
});

imap.once('error', (err) => {
    console.log(err);
});

imap.once('end', () => {
    console.log('Connection ended');
});

imap.connect();